import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

plugins {
    kotlin("jvm") version Version.kotlin
    kotlin("plugin.compose") version Version.kotlin apply false
    id("com.google.devtools.ksp") version Version.kspVersion apply false
    id("org.jetbrains.compose") version Version.compose apply false
}

group = Group.groupId
version = Group.version

allprojects {
    repositories {
        mavenCentral()
        google()
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
    }
}
buildscript {
    dependencies {
        classpath(kotlin("gradle-plugin", version = Version.kotlin))
    }

}
tasks.withType<KotlinJvmCompile> {
    compilerOptions.jvmTarget.set(JvmTarget.JVM_17)
}
