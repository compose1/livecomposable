# LiveComposable (Name not final!)

## Steps to use @HotReload in your own project.
overview:
* add gradle plugin, repositories and dependencies
* add gradle.properties entry
* add .gitignore folder

### Gradle
in your build.gradle.kts file add following entries:
```kotlin
plugins {
    id("com.google.devtools.ksp") version "2.0.20-1.0.24"
}

repositories {
    google()
    // temporary solution until it is published on mavenCentral
    maven { url = uri("https://apps-on-air.de/maven/livecomposable") }
}

dependencies {
    val version = "0.3.3-1.6.11"
    implementation("de.drick.compose:hotreload:$version")
    implementation("de.drick.compose:hotreload-ksp-processor:$version")
    ksp("de.drick.compose:hotreload-ksp-processor:$version")
}
```

### Gitignore
The folder runtime is created from your running application and should not be commited to git.
Add folder `runtime/` to .gitignore
