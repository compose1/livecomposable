# LiveComposable (Name not final!)

## Description
Inspired from the orx-olive module of openrndr which allows the developers to change code during runtime and execute it directly. (https://github.com/openrndr/orx)
I wanted to have this feature in Compose for Desktop.

## Live demo
![Live demo](doc/screencast1.mp4)

## Usage
Follow the [setup guide](doc/SETUP.md) 

Just annotate the @Composable function with the additional @HotReload annotation.
E.g.: file: testHotReload.kt
```kotlin
@HotReload
@Composable
fun SimpleUI() {
    //my compose UI
    //...
}
```
Then the function `<function name>HotReload` is generated. It can be used to execute this function with hot reload capabilities.
This means that the source kotlin file is observed for changes and every time when a change is detected it will be compiled and loaded again.

file: main.kt
```kotlin
fun main() {
    Window() {
        SimpleUIHotReload()
    }
}

```

The complete file can be changed except the @HotReload annotated function signature itself. You can define as many other functions in this file as you want, and you can also change the @HotReload function except the signature of it. (Keep in mind that only this one file is recompiled on changes)


## Limitations

* The annotated function must be outside a class.
* The annotated function parameters must NOT contain types which are defined in the same file.
* Only the file where the annotation is positioned will be recompiled and reloaded.
* Currently, it is not possible to chain hot reload function calls.
* Compilation time is not faster than compiling it in IntelliJ (But only one file is compiled).
* Many more I am not aware of :-D

Some limitations could be eliminated by putting some more work in it others are hard and will stay.

## How it works

To better understand the possibilities and also limitations of this library it is necessary to understand how it works.
When a function is annotated with @HotReload the compiler plugin "hotreload_ksp_processor" will create a class in the same package which is used to load during runtime and forward the calls to the hot reload function. In addition a function with the same name like the hot reload function but with a suffix of "HotReload" is created which will observe changes of the source file. When a change is detected it will compile the file and load the class during runtime. It also renders the compose code.

## Known issues

* Issue with KSP generated source files are not detected by IDE. Workaround: Manually define the generated folder ("generated/ksp/main/kotlin") as source folders.

## License
```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <https://unlicense.org>
```
