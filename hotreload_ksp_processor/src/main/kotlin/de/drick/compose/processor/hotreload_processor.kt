package de.drick.compose.processor

import com.google.devtools.ksp.processing.*
import com.google.devtools.ksp.symbol.*
import de.drick.compose.live_composable.HotReload
import java.io.OutputStream

data class FunParam(val name: String, val type: String)

fun classTemplate(
    className: String,
    packageName: String,
    annotations: String,
    funName: String,
    params: List<FunParam>,
    imports: List<String>
) = """
${if (packageName.isEmpty()) "" else "package $packageName"}

${imports.joinToString("\n")}

class $className : ${className}_Interface() {
    $annotations
    override fun hotReload(${params.joinToString(", ") { "${it.name}: ${it.type}" }}) {
        $funName(${params.joinToString(", ") { it.name }})
    }
}
""".trimIndent()


fun interfaceTemplate(
    classFile: String,
    interfaceFile: String,
    className: String,
    packageName: String,
    annotations: String,
    funName: String,
    params: List<FunParam>,
    imports: List<String>
) = """
${if (packageName.isEmpty()) "" else "package $packageName"}

${imports.joinToString("\n")}

abstract class ${className}_Interface {    
    $annotations
    abstract fun hotReload(${params.joinToString(", ") { "${it.name}: ${it.type}" }})
}

$annotations
fun ${funName}HotReload(${params.joinToString(", ") { "${it.name}: ${it.type}" }}) {
    de.drick.compose.live_composable.LiveComposable<${className}, ${className}_Interface>(
        hotReloadFile = "$interfaceFile",
        interfaceFile = "$classFile"
    ) {
        hotReload(${params.joinToString(", ") { it.name }})
    }
}
""".trimIndent()

class HotReloadKspProvider : SymbolProcessorProvider {
    override fun create(
        environment: SymbolProcessorEnvironment
    ) = HotReloadKspProcessor(environment.codeGenerator, environment.logger)
}

class HotReloadKspProcessor(
    private val codeGenerator: CodeGenerator,
    private val logger: KSPLogger
) : SymbolProcessor {
    override fun process(resolver: Resolver): List<KSAnnotated> {
        logger.info("Start processing")
        val symbols = resolver.getSymbolsWithAnnotation(HotReload::class.qualifiedName.toString())
        symbols.filterIsInstance<KSFunctionDeclaration>().forEach {
            when {
                it.parentDeclaration != null ->
                    logger.error("@HotReload annotation must not be used inside of a class!", it)
                it.annotations.find { it.shortName.asString() == "Composable" } == null ->
                    logger.error("@HotReload annotation can only be used on @Composable functions!", it)
                else -> it.accept(HotReloadKspVisitor(), Unit)
            }
        }
        return emptyList()
    }

    inner class HotReloadKspVisitor : KSVisitorVoid() {
        override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit) {
            classDeclaration.primaryConstructor!!.accept(this, data)
        }

        override fun visitFunctionDeclaration(function: KSFunctionDeclaration, data: Unit) {
            logger.info("Processing function: $function", function)
            val sourceFile = requireNotNull(function.containingFile)
            val dependencies = Dependencies(true, sourceFile)
            val tmpPkg = sourceFile.packageName.asString()
            val packageName = if (tmpPkg == "<root>") "" else tmpPkg
            val funName = function.simpleName.getShortName()
            val className = "HotReloadClass_$funName"
            val importSet = mutableSetOf<String>()
            function.parameters.forEach { parameter ->
                val typeArgs = parameter.type.element!!.typeArguments
                if (parameter.type.element!!.typeArguments.isNotEmpty()) {
                    typeArgs.forEach { typeArg ->
                        val type = typeArg.type?.resolve()
                        type?.declaration?.qualifiedName?.asString()?.let { fullQualifiedTypeName: String ->
                            importSet.add("import $fullQualifiedTypeName")
                        }
                    }
                }
            }
            val imports = importSet.toList()
            val params = function.parameters.map {
                FunParam(requireNotNull(it.name?.asString()), it.type.toString())
            }
            val annotations = "@androidx.compose.runtime.Composable"
            val classFileName = className
            // create HotreloadClass_<FunName>
            codeGenerator.createNewFile(dependencies, packageName, classFileName).use {
                it.writeText(
                    classTemplate(
                        className = className,
                        packageName = packageName,
                        annotations = annotations,
                        funName = funName,
                        params = params,
                        imports = imports
                    )
                )
            }
            val file = checkNotNull(codeGenerator.generatedFile.find { it.nameWithoutExtension == classFileName }) { "Expected file not created! $classFileName" }
            // create interface class
            codeGenerator.createNewFile(dependencies, packageName, "${classFileName}_${funName}_interface").use {
                it.writeText(
                    interfaceTemplate(
                        classFile = file.absolutePath,
                        interfaceFile = sourceFile.filePath,
                        className = className,
                        packageName = packageName,
                        annotations = annotations,
                        funName = funName,
                        params = params,
                        imports = imports
                    )
                )
            }
        }
    }

}

fun OutputStream.writeText(str: String) {
    this.write(str.toByteArray())
}