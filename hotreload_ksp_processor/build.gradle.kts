plugins {
    kotlin("jvm")
    id("maven-publish")
}

repositories {
    google()
}

dependencies {
    implementation(project(":hotreload_annotation"))
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))

    implementation("com.google.devtools.ksp:symbol-processing-api:${Version.kspVersion}")
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("hotreload-annotation") {
            groupId = Group.groupId
            version = Group.version
            artifactId = "hotreload-ksp-processor"

            from(components["kotlin"])
            artifact(tasks.getByName("sourcesJar"))
        }
    }

    repositories {
        maven {
            setUrl("$rootDir/maven_repo")
        }
    }
}
