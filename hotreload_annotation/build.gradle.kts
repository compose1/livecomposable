plugins {
    kotlin("jvm")
    id("maven-publish")
}

dependencies {
    implementation(kotlin("stdlib"))
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("hotreload-annotation") {
            groupId = Group.groupId
            version = Group.version
            artifactId = "hotreload-annotation"

            from(components["kotlin"])
            artifact(tasks.getByName("sourcesJar"))
        }
    }

    repositories {
        maven {
            setUrl("$rootDir/maven_repo")
        }
    }
}
