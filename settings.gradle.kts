pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
        maven { url = uri("https://maven.pkg.jetbrains.space/public/p/compose/dev") }
    }
}
rootProject.name = "LiveComposable"

include(":sample")
include(":hotreload")
include(":hotreload_ksp_processor")
//include(":hotreload_processor")
include(":hotreload_annotation")
