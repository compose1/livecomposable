object Version {
    const val kotlin = "2.0.20"
    const val compose = "1.6.11"
    const val kspVersion= "2.0.20-1.0.24"
}

object Group {
    const val groupId = "de.drick.compose"
    const val version = "0.3.3-1.6.11"
}
