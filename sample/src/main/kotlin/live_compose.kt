import androidx.compose.foundation.Canvas
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import de.drick.compose.live_composable.HotReload
import kotlin.math.sin

@Composable
@HotReload()
fun App(seconds: Double) {
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.TopCenter) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            val text = remember { mutableStateOf("Hello World") }
            Text(text.value, Modifier.padding(8.dp))
            Box(Modifier.fillMaxWidth().weight(1f).padding(32.dp).clip(RoundedCornerShape(8.dp)).border(4.dp, Color.Blue, RoundedCornerShape(8.dp))) {
                Canvas(Modifier.fillMaxSize()) {
                    val radius = sin(seconds).toFloat() + 1f
                    drawCircle(Color.Green)
                    drawCircle(Color.LightGray, radius = radius * 100f, center = Offset(160f, 300f))
                    drawCircle(Color.Red, radius = 150f, center = Offset(radius * 400f, 300f))
                }
            }
            Spacer(Modifier.height(8.dp))
            Button(onClick = { text.value = "Button 1 pressed" }) {
                Text("Press button 1")
            }
            Spacer(Modifier.height(8.dp))
            Button(onClick = { text.value = "Button 2 pressed" }) {
                Text("Press button 2")
            }
            Spacer(Modifier.height(8.dp))
        }
    }
}
