package test.pack

import Action
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import de.drick.compose.live_composable.HotReload

private val lightPrimaryColor = Color(0xFFFAB24A)
private val darkPrimaryColor = Color(0xFF7D4433)
private val textColorOnLight = Color.Black
private val textColorOnDark = Color.White

private val primary = Color(0xFFF49F0D)
private val primaryVariant = Color(0xFFFABE5D)
private val activeInverted = Color.White


private val secondary = Color(0xFF894E34)
private val secondaryVariant = Color(0xFF915E36)

private val foreground = Color(0xFF894E34)

private val background = Color(0xFFFEEED2)
private val backgroundVariant = Color(0xFFFAC367)

@HotReload
@Composable
fun SimpleUI(onContinue: (Action) -> Unit) {
    val spacing = 8.dp
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background),
        contentAlignment = Alignment.Center
    ) {
        Column(
            Modifier.padding(horizontal = 16.dp).widthIn(max = 400.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text("Hello World", style = MaterialTheme.typography.h3)
            Spacer(Modifier.height(32.dp))
            StartButton("Puppy", onClick = { onContinue(Action.Puppy) })
            Spacer(Modifier.width(spacing))
            StartButton("CountPoser", onClick = { onContinue(Action.CountPoser) })
            Spacer(Modifier.width(spacing))
            StartButton("Continue", onClick = { onContinue(Action.Continue) }, true)
        }
    }
}

@Composable
fun StartButton(title: String, onClick: () -> Unit, primary: Boolean = false) {
    Button(
        onClick = onClick,
        modifier = Modifier
            .height(52.dp),
        shape = MaterialTheme.shapes.medium,
        colors = ButtonDefaults.buttonColors(
            if (primary) MaterialTheme.colors.primary
            else MaterialTheme.colors.secondary
        )
    ) {
        Text(text = title.uppercase())
    }
}