import androidx.compose.ui.window.singleWindowApplication
import test.pack.SimpleUI
import test.pack.SimpleUIHotReload

enum class Action { Puppy, CountPoser, Continue }

fun main() {
   singleWindowApplication() {
        SimpleUIHotReload {
            println(it)
        }
    }
}
