import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    id("com.google.devtools.ksp")
    kotlin("jvm")
    id("org.jetbrains.compose")
    kotlin("plugin.compose")
}

repositories {
    google()
}

dependencies {
    implementation(project(":hotreload"))
    implementation(project(":hotreload_ksp_processor"))
    ksp(project(":hotreload_ksp_processor"))

    implementation(compose.desktop.currentOs)
}

compose.desktop {
    application {
        mainClass = "MainKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "LiveComposable"
            packageVersion = "1.0.0"
        }
    }
}