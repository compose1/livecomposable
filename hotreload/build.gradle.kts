plugins {
    kotlin("jvm")
    kotlin("plugin.compose")
    id("org.jetbrains.compose")
    id("maven-publish")
}

dependencies {
    api(project(":hotreload_annotation"))
    implementation(compose.desktop.common)
    implementation(kotlin("reflect"))
    implementation(kotlin("compiler-embeddable"))
}

val generatedResources = "${layout.buildDirectory}/resources/main"

sourceSets {
    main {
        output.dir(generatedResources, "builtBy" to "copyComposeCompiler")
    }
}

tasks.register<Copy>("copyComposeCompiler") {
    val pluginClasspath = project.configurations.kotlinCompilerPluginClasspathMain
    val composePluginPath = pluginClasspath.get().files.single {
        it.toString().contains("compose-compiler-plugin")
    }
    println(composePluginPath)

    from(composePluginPath)
    into(file("$generatedResources/compilerPlugins"))
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("hotreload-annotation") {
            groupId = Group.groupId
            version = Group.version
            artifactId = "hotreload"

            from(components["kotlin"])
            artifact(tasks.getByName("sourcesJar"))
        }
    }

    repositories {
        maven {
            setUrl("$rootDir/maven_repo")
        }
    }
}
